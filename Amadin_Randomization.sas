﻿/*** Заменить следующие три переменные для финального рандомизационного листа ***/

*%LET type = ФИКТИВНЫЙ; /* Изменить на "ФИНАЛЬНЫЙ" без кавычек для финального рандомизационного листа */
%LET type = ФИНАЛЬНЫЙ; /* Изменить на "ФИКТИВНЫЙ" без кавычек для фиктивного рандомизационного листа */
%LET path = C:/CORP_PRJ/PRJ_imishchenko/Projects/Amadin/Rand_Output; /* папка, куда будут выводиться документы, и где хранится логотип */
%LET seed = 184623; /* аргумент рандомизации, заменить на произвольное число для финального рандомизационного листа */

/*** Конец блока заменяемых переменных ***/

%LET prjcode = Amadin; /* код проекта */
%LET arms = 2; /* количество групп */
%LET NPOP = 40; /* размер популяции */
%LET Narm = 20; /* = NPOP/arms -- количество пациентов в каждой группе */

%LET N = 48; /* количество рандомизационных номеров */
%LET blocksize = 4; /* фиксированный размер блока */
%LET randnum = 1; /* первый рандомизационный номер, остальные присваиваются последовательно */
*%LET flasknum = 1; /* первый номер флакона, остальные присваиваются последовательно */

%let curprog = %sysget(SAS_EXECFILEPATH); 
%let time = %sysfunc(time(),time8.0); 
%let date = %sysfunc(date(),date9.); 

data _NULL_;
call symput ('TypeProp', propcase("&type"));
run;

proc format;
value arm 
1 = "БАД Амадин 700 мг"
2 = "Кагоцел® 12 мг";
run;


%MACRO rand (seed, N, blocksize, randnum, arms);

proc plan seed = &seed;
factors BLOCK = %sysfunc(ceil(&N/&blocksize)) random ARM = &blocksize;
output out = Blocks_raw;
run;

data Blocks_final; 
set Blocks_raw;
ITEM = mod(_N_ - 1, &blocksize) + 1;
Retain RANDNUM &randnum;
if _N_ > 1 then randnum + 1;
ARM = mod(Arm,&arms) + 1;
run;

%mend rand;

%rand(&seed, &N, &blocksize, &randnum, &arms);

options nodate nonumber byline orientation=portrait papersize=letter
bottommargin = 0.4in
Topmargin = 0.4in
leftmargin = 1.2in
rightmargin = 0.8in;

ods escapechar = "^";
ods listing close;


ods rtf file = "&path\Amadin_RandomizationList_&TypeProp..rtf" startpage=no;

title j = r '^S={preimage="&path\logo10.png" borderbottomcolor=black borderbottomwidth=2pt outputwidth=100%}'  /* ! ' ... "" ... ' ! */
j = l "^S={font_size=8pt borderbottomcolor=black borderbottomwidth=2pt outputwidth=100%}";

footnote j = l "^S={font_size=8pt }SF-DM&BS-12.05-01 (effective date 01-Sep-2017)
^n Atlant Clinical Ltd.
^n SAS version &SYSVER for &SYSSCP &sysscpl
^n Date: &date Time: &time
^n &curprog"
j = r "^S={font_size=8pt }Page ^{thispage} of ^{lastpage}";

data header;
text = "^S={font_size=16pt just=c}&type РАНДОМИЗАЦИОННЫЙ ЛИСТ^n (код проекта &prjcode)";
run;

proc report data=header nowd noheader style(report)={rules=none frame=void} 
style(column)={font_weight=bold font_size=20pt just=c};
run;

title j = l "^S={font_size=11pt}&TypeProp рандомизационный лист ^n Cпонсор: ООО ^{unicode 0022}^S={font_size=11pt}БИО 8^{unicode 0022}"; * ^n ^S={font_size=11pt}Протокол: NA ;
*			" &TypeProp ... ^{unicode 0022} ... ^{unicode 0022}" ;

ods rtf text='^S={font_size=12pt font_weight=bold just=l}Cпонсор: ^S={font_size=12pt just=l} ООО "БИО 8"';
ods rtf text="^S={font_size=12pt font_weight=bold just=l}Исследуемый препарат: ^S={font_size=12pt just=l} Амадин, биологически активная добавка, таблетки по 700 мг";
ods rtf text="^S={font_size=12pt font_weight=bold just=l}Тип исследования: ^S={font_size=12pt just=l} Поисковое исследование";
ods rtf text="^S={font_size=12pt font_weight=bold just=l}Название исследования: ^S={font_size=12pt just=l} Поисковое исследование клинического эффекта биологически активной добавки Амадин при приеме у пациентов с ОРВИ или гриппом легкой и средней степени тяжести на фоне стандартной симптоматической терапии";
*ods rtf text="^S={font_size=12pt font_weight=bold just=l}Номер протокола: ^S={font_size=12pt just=l} Неприменимо";
ods rtf text="^S={font_size=12pt font_weight=bold just=l}Дизайн исследования: ^S={font_size=12pt just=l} Одноцентровое открытое рандомизированное поисковое исследование в двух группах";
ods rtf text="^S={font_size=12pt font_weight=bold just=l}Рандомизация: ^S={font_size=12pt just=l} 1:1 (700 мг БАД Амадин : 12 мг Кагоцел®)";
ods rtf text="^S={font_size=12pt font_weight=bold just=l}Количество групп: ^S={font_size=12pt just=l} &arms";
ods rtf text="^S={font_size=12pt font_weight=bold just=l}Максимальное планируемое количество человек в каждой группе: ^S={font_size=12pt just=l} &Narm";
ods rtf text="^S={font_size=12pt font_weight=bold just=l}Максимальное планируемое количество человек в исследовании: ^S={font_size=12pt just=l} &NPOP";
*ods rtf text = " ";
*ods rtf text="^S={font_size=12pt font_weight=bold just=l}Препарат XC221 100 мг: ^S={font_size=12pt just=l}150 флаконов по 1 таблетке в каждом (Всего 150 таблеток) ";
*ods rtf text="^S={font_size=12pt font_weight=bold just=l}Плацебо 100 мг: ^S={font_size=12pt just=l}150 флаконов по 1 таблетке в каждом (Всего 150 таблеток) ";
ods rtf text = " ";
ods rtf text="^S={font_size=12pt just=l}Пациенты будут рандомизированы в &arms группы в соотношении 1:1 для получения исследуемого препарата / препарата сравнения на фоне стандартной симптоматической терапии:";
ods rtf text = " ";
ods rtf text="^S={font_size=12pt font_weight=bold just=l leftmargin=10pt}Группа 700 мг БАД Амадин - ^S={font_size=12pt just=l} будет назначен исследуемый БАД Амадин в дозировке 700 мг по 1 таблетке 2 раза в день на фоне стандартной симптоматической терапии (&Narm пациентов);";
ods rtf text="^S={font_size=12pt font_weight=bold just=l leftmargin=10pt}Группа 12 мг Кагоцел® - ^S={font_size=12pt just=l} будет назначен препарат сравнения Кагоцел® в дозировке 12 мг по 2 таблетки 3 раза в день в первые два дня и по 1 таблетке 3 раза в день в последующие два дня на фоне стандартной симптоматической терапии (&Narm пациентов).";
ods rtf text = " ";
*ods rtf text="^S={font_size=12pt font_weight=bold just=l leftmargin=15pt}Набор для 1 пациента: ^S={font_size=12pt just=l}Для каждого пациента будет предназначен набор из 6 флаконов, в каждый флакон помещена 1 таблетка (исследуемый препарат или плацебо). Каждый флакон будет пронумерован для соответствующего дня приема: «День 1», «День 2», «День 3».";
*ods rtf text = " ";
*ods rtf text="^S={font_size=12pt font_weight=bold just=l leftmargin=15pt}Схема применения: ^S={font_size=12pt just=l} Принимается внутрь, 1 раз в день из соответствующего флакона по 1 таблетке (итого – 2 таблетки в сутки) независимо от приема пищи в течение 3 дней. ";
*ods rtf text = " ";

*ods rtf text="^S={font_size=12pt just=l textdecoration=underline}Пациенты, которым будет назначен исследуемый препарат ХС221 в суточной дозе 100 мг,^S={font_size=12pt just=l} будут получать в течение 3 дней каждый день по 2 таблетки в следующей комбинации: 1 таблетка исследуемого препарата ХС221 100 мг + 1 таблетка плацебо 100 мг. Итого пациент будет принимать по 2 таблетки каждый день в течение 3 дней. ";
*ods rtf text = " ";
*ods rtf text="^S={font_size=12pt just=l textdecoration=underline}Пациенты, которым будет назначен исследуемый препарат ХС221 в суточной дозе 200 мг,^S={font_size=12pt just=l} будут получать в течение 3 дней каждый день по 2 таблетки в следующей комбинации: 1 таблетка исследуемого препарата ХС221 100 мг + 1 таблетка исследуемого препарата ХС221 100 мг. Итого пациент будет принимать по 2 таблетки каждый день в течение 3 дней. ";
*ods rtf text = " ";
*ods rtf text="^S={font_size=12pt just=l textdecoration=underline}Пациенты, которым будет назначено плацебо,^S={font_size=12pt just=l} будут получать в течение 3 дней каждый день по 2 таблетки в следующей комбинации: 1 таблетка плацебо 100 мг + 1 таблетка плацебо 100 мг. Итого пациент будет принимать по 2 таблетки каждый день в течение 3 дней. ";
*ods rtf text = " ";
*ods rtf text="^S={font_size=12pt just=l}Рандомизация проведена с учетом подготовки 150 наборов (по 40 наборов для каждого типа лечения + по 10 запасных наборов). ";
*ods rtf text = " ";

ods startpage=now;

ods rtf text = " ";
ods rtf text = "^S={font_size=16pt just=l leftmargin=5pt} Параметры рандомизации:";
ods rtf text = " ";
ods rtf text = " ";
ods rtf text = "^S={font_size=12pt font_weight=bold just=l}Количество групп: &arms";
ods rtf text = "^S={font_size=12pt font_weight=bold just=l}Планируемое количество пациентов: &NPOP";
ods rtf text = "^S={font_size=12pt font_weight=bold just=l}Количество рандомизационных номеров: &N";
ods rtf text = "^S={font_size=12pt font_weight=bold just=l}Размер блока: &blocksize";
ods rtf text = "^S={font_size=12pt font_weight=bold just=l}Рандомизационный номер первого пациента: &randnum";
ods rtf text = "^S={font_size=12pt font_weight=bold just=l}Seed: &seed";
ods rtf text = " ";

proc print data=blocks_final noobs
style(header)={just=c}
style(column)={width=100pt just=c};
var BLOCK ITEM RANDNUM ARM;
format RANDNUM Z2. ARM arm.;
run;

ods startpage=now;

ods rtf text="^S={font_size=16pt font_weight=bold just=c}&type РАНДОМИЗАЦИОННЫЙ ЛИСТ: Подтверждение о получении ^n
(код проекта &prjcode)";

ods rtf text = "^n^n ";

ods rtf text = "^S={fontstyle=italic font_size=13pt}Рандомизационный лист для клинических исследований должен надежно храниться Спонсором или консультантом (например, представителем 
комитета по мониторингу данных) таким образом, чтобы гарантировать ограниченный доступ только уполномоченным лицам, и заслепление будет поддерживаться в течение всего клинического 
исследования. Любая передача Рандомизационного листа, его электронной или бумажной копии, должна быть выполнена надежно и задокументирована (что передано, кем, кому, когда).";

ods rtf text = " ";
ods rtf text = " ";

ods rtf text = "^S={fontstyle=italic font_size=13pt}Рандомизационный лист составлен в соответствии с OP-DM&BS-12 и передан получателю ";

ods rtf text = " ";

ods rtf text = " ";
ods rtf text="^S={font_size=13pt font_weight=bold just=l fontstyle=italic}Подпись: ^S={font_size=13pt just=l} ____________________________ ";
ods rtf text = " ";
ods rtf text="^S={font_size=13pt font_weight=bold just=l fontstyle=italic}ФИО: ^S={font_size=13pt just=l} ________________________________ ";
ods rtf text = " ";
ods rtf text="^S={font_size=13pt font_weight=bold just=l fontstyle=italic}Должность: ^S={font_size=13pt just=l} _________________________ ";
ods rtf text = " ";
ods rtf text="^S={font_size=13pt font_weight=bold just=l}Дата: ^S={font_size=13pt just=l} ________________________________";

ods rtf text = " ";
ods rtf text = " ";
ods rtf text = " ";

ods rtf text = "^S={fontstyle=italic font_size=13pt}Рандомизационный лист получен ";

ods rtf text = " ";

ods rtf text = " ";
ods rtf text="^S={font_size=13pt font_weight=bold just=l fontstyle=italic}Подпись: ^S={font_size=13pt just=l} ____________________________ ";
ods rtf text = " ";
ods rtf text="^S={font_size=13pt font_weight=bold just=l fontstyle=italic}ФИО: ^S={font_size=13pt just=l} ________________________________ ";
ods rtf text = " ";
ods rtf text="^S={font_size=13pt font_weight=bold just=l fontstyle=italic}Должность: ^S={font_size=13pt just=l} _________________________ ";
ods rtf text = " ";
ods rtf text="^S={font_size=13pt font_weight=bold just=l}Дата: ^S={font_size=13pt just=l} ________________________________";

ods _all_ close;
ods listing;


ods pdf file = "&path\Amadin_RandomizationList_&TypeProp..pdf" startpage=no;

title j = r '^S={preimage="&path\logo10.png" borderbottomcolor=black borderbottomwidth=2pt outputwidth=100%}'
j = l "^S={font_size=8pt borderbottomcolor=black borderbottomwidth=2pt outputwidth=100%}";
footnote j = l "^S={font_size=8pt }SF-DM&BS-12.05-01 (effective date 01-Sep-2017)
^n Atlant Clinical Ltd.
^n SAS version &SYSVER for &SYSSCP &sysscpl
^n Date: &date Time: &time
^n &curprog"
j = r "^S={font_size=8pt }Page ^{thispage} of ^{lastpage}";

data header;
text = "^S={font_size=16pt just=c}&type РАНДОМИЗАЦИОННЫЙ ЛИСТ^n (код проекта &prjcode)";
run;

proc report data=header nowd noheader style(report)={rules=none frame=void} 
style(column)={font_weight=bold font_size=20pt just=c};
run;

title j = l "^S={font_size=11pt}&TypeProp рандомизационный лист ^n Cпонсор: ООО ^{unicode 0022}^S={font_size=11pt}БИО 8^{unicode 0022}"; * ^n ^S={font_size=11pt}Протокол: NA ;

ods pdf text='^S={font_size=12pt font_weight=bold just=l}Cпонсор: ^S={font_size=12pt just=l} ООО "БИО 8"';
ods pdf text="^S={font_size=12pt font_weight=bold just=l}Исследуемый препарат: ^S={font_size=12pt just=l} Амадин, биологически активная добавка, таблетки по 700 мг";
ods pdf text="^S={font_size=12pt font_weight=bold just=l}Тип исследования: ^S={font_size=12pt just=l} Поисковое исследование";
ods pdf text="^S={font_size=12pt font_weight=bold just=l}Название исследования: ^S={font_size=12pt just=l} Поисковое исследование клинического эффекта биологически активной добавки Амадин при приеме у пациентов с ОРВИ или гриппом легкой и средней степени тяжести на фоне стандартной симптоматической терапии";
*ods pdf text="^S={font_size=12pt font_weight=bold just=l}Номер протокола: ^S={font_size=12pt just=l} Неприменимо";
ods pdf text="^S={font_size=12pt font_weight=bold just=l}Дизайн исследования: ^S={font_size=12pt just=l} Одноцентровое открытое рандомизированное поисковое исследование в двух группах";
ods pdf text="^S={font_size=12pt font_weight=bold just=l}Рандомизация: ^S={font_size=12pt just=l} 1:1 (700 мг БАД Амадин : 12 мг Кагоцел®)";
ods pdf text="^S={font_size=12pt font_weight=bold just=l}Количество групп: ^S={font_size=12pt just=l} &arms";
ods pdf text="^S={font_size=12pt font_weight=bold just=l}Максимальное планируемое количество человек в каждой группе: ^S={font_size=12pt just=l} &Narm";
ods pdf text="^S={font_size=12pt font_weight=bold just=l}Максимальное планируемое количество человек в исследовании: ^S={font_size=12pt just=l} &NPOP";
*ods pdf text = " ";
*ods pdf text="^S={font_size=12pt font_weight=bold just=l}Препарат XC221 100 мг: ^S={font_size=12pt just=l}150 флаконов по 1 таблетке в каждом (Всего 150 таблеток) ";
*ods pdf text="^S={font_size=12pt font_weight=bold just=l}Плацебо 100 мг: ^S={font_size=12pt just=l}150 флаконов по 1 таблетке в каждом (Всего 150 таблеток) ";
ods pdf text = " ";
ods pdf text="^S={font_size=12pt just=l}Пациенты будут рандомизированы в &arms группы в соотношении 1:1 для получения исследуемого препарата / препарата сравнения на фоне стандартной симптоматической терапии:";
ods pdf text = " ";
ods pdf text="^S={font_size=12pt font_weight=bold just=l leftmargin=10pt}Группа 700 мг БАД Амадин - ^S={font_size=12pt just=l} будет назначен исследуемый БАД Амадин в дозировке 700 мг по 1 таблетке 2 раза в день на фоне стандартной симптоматической терапии (&Narm пациентов);";
ods pdf text="^S={font_size=12pt font_weight=bold just=l leftmargin=10pt}Группа 12 мг Кагоцел® - ^S={font_size=12pt just=l} будет назначен препарат сравнения Кагоцел® в дозировке 12 мг по 2 таблетки 3 раза в день в первые два дня и по 1 таблетке 3 раза в день в последующие два дня на фоне стандартной симптоматической терапии (&Narm пациентов).";
ods pdf text = " ";
*ods pdf text="^S={font_size=12pt font_weight=bold just=l leftmargin=15pt}Набор для 1 пациента: ^S={font_size=12pt just=l}Для каждого пациента будет предназначен набор из 6 флаконов, в каждый флакон помещена 1 таблетка (исследуемый препарат или плацебо). Каждый флакон будет пронумерован для соответствующего дня приема: «День 1», «День 2», «День 3».";
*ods pdf text = " ";
*ods pdf text="^S={font_size=12pt font_weight=bold just=l leftmargin=15pt}Схема применения: ^S={font_size=12pt just=l} Принимается внутрь, 1 раз в день из соответствующего флакона по 1 таблетке (итого – 2 таблетки в сутки) независимо от приема пищи в течение 3 дней. ";
*ods pdf text = " ";

*ods pdf text="^S={font_size=12pt just=l textdecoration=underline}Пациенты, которым будет назначен исследуемый препарат ХС221 в суточной дозе 100 мг,^S={font_size=12pt just=l} будут получать в течение 3 дней каждый день по 2 таблетки в следующей комбинации: 1 таблетка исследуемого препарата ХС221 100 мг + 1 таблетка плацебо 100 мг. Итого пациент будет принимать по 2 таблетки каждый день в течение 3 дней. ";
*ods pdf text = " ";
*ods pdf text="^S={font_size=12pt just=l textdecoration=underline}Пациенты, которым будет назначен исследуемый препарат ХС221 в суточной дозе 200 мг,^S={font_size=12pt just=l} будут получать в течение 3 дней каждый день по 2 таблетки в следующей комбинации: 1 таблетка исследуемого препарата ХС221 100 мг + 1 таблетка исследуемого препарата ХС221 100 мг. Итого пациент будет принимать по 2 таблетки каждый день в течение 3 дней. ";
*ods pdf text = " ";
*ods pdf text="^S={font_size=12pt just=l textdecoration=underline}Пациенты, которым будет назначено плацебо,^S={font_size=12pt just=l} будут получать в течение 3 дней каждый день по 2 таблетки в следующей комбинации: 1 таблетка плацебо 100 мг + 1 таблетка плацебо 100 мг. Итого пациент будет принимать по 2 таблетки каждый день в течение 3 дней. ";
*ods pdf text = " ";
*ods pdf text="^S={font_size=12pt just=l}Рандомизация проведена с учетом подготовки 150 наборов (по 40 наборов для каждого типа лечения + по 10 запасных наборов). ";
*ods pdf text = " ";

ods startpage=now;

ods pdf text = " ";
ods pdf text="^S={font_size=16pt just=l leftmargin=5pt} Параметры рандомизации:";
ods pdf text = " ";
ods pdf text = " ";
ods pdf text = "^S={font_size=12pt font_weight=bold just=l}Количество групп: &arms";
ods pdf text = "^S={font_size=12pt font_weight=bold just=l}Планируемое количество пациентов: &NPOP";
ods pdf text = "^S={font_size=12pt font_weight=bold just=l}Количество рандомизационных номеров: &N";
ods pdf text = "^S={font_size=12pt font_weight=bold just=l}Размер блока: &blocksize";
ods pdf text = "^S={font_size=12pt font_weight=bold just=l}Рандомизационный номер первого пациента: &randnum";
ods pdf text = "^S={font_size=12pt font_weight=bold just=l}Seed: &seed";
ods pdf text = " ";

proc print data=blocks_final noobs
style(header)={just=c}
style(column)={width=100pt just=c};
var BLOCK ITEM RANDNUM ARM;
format RANDNUM Z2. ARM arm.;
run;

ods startpage=now;

ods pdf text="^S={font_size=16pt font_weight=bold just=c}&type РАНДОМИЗАЦИОННЫЙ ЛИСТ: Подтверждение о получении ^n
(код проекта &prjcode)";

ods pdf text = "^n^n ";

ods pdf text = "^S={fontstyle=italic font_size=13pt}Рандомизационный лист для клинических исследований должен надежно храниться Спонсором или консультантом (например, представителем 
комитета по мониторингу данных) таким образом, чтобы гарантировать ограниченный доступ только уполномоченным лицам, и заслепление будет поддерживаться в течение всего клинического 
исследования. Любая передача Рандомизационного листа, его электронной или бумажной копии, должна быть выполнена надежно и задокументирована (что передано, кем, кому, когда).";

ods pdf text = " ";
ods pdf text = " ";

ods pdf text = "^S={fontstyle=italic font_size=13pt}Рандомизационный лист составлен в соответствии с OP-DM&BS-12 и передан получателю ";

ods pdf text = " ";

ods pdf text = " ";
ods pdf text="^S={font_size=13pt font_weight=bold just=l fontstyle=italic}Подпись: ^S={font_size=13pt just=l} ____________________________ ";
ods pdf text = " ";
ods pdf text="^S={font_size=13pt font_weight=bold just=l fontstyle=italic}ФИО: ^S={font_size=13pt just=l} ________________________________ ";
ods pdf text = " ";
ods pdf text="^S={font_size=13pt font_weight=bold just=l fontstyle=italic}Должность: ^S={font_size=13pt just=l} _________________________ ";
ods pdf text = " ";
ods pdf text="^S={font_size=13pt font_weight=bold just=l}Дата: ^S={font_size=13pt just=l} ________________________________";

ods pdf text = " ";
ods pdf text = " ";
ods pdf text = " ";

ods pdf text = "^S={fontstyle=italic font_size=13pt}Рандомизационный лист получен ";

ods pdf text = " ";

ods pdf text = " ";
ods pdf text="^S={font_size=13pt font_weight=bold just=l fontstyle=italic}Подпись: ^S={font_size=13pt just=l} ____________________________ ";
ods pdf text = " ";
ods pdf text="^S={font_size=13pt font_weight=bold just=l fontstyle=italic}ФИО: ^S={font_size=13pt just=l} ________________________________ ";
ods pdf text = " ";
ods pdf text="^S={font_size=13pt font_weight=bold just=l fontstyle=italic}Должность: ^S={font_size=13pt just=l} _________________________ ";
ods pdf text = " ";
ods pdf text="^S={font_size=13pt font_weight=bold just=l}Дата: ^S={font_size=13pt just=l} ________________________________";

ods _all_ close;
ods listing;

data numlist;
do RANDNUM=&randnum to &N+&randnum-1;
x = ranuni(&seed);
output;
end;
run;

proc sort data=numlist;
by x;
run;

proc sort data=flasklist;
by x;
run;
